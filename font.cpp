#include "font.h"
#include "ft2build.h"
#include FT_FREETYPE_H
#include FT_GLYPH_H


/* 字体数据（ttf） */
typedef struct _ft_fontinfo {
	FT_Face    face;     /* FreeType库句柄对象 */
	FT_Library library;  /* 外观对象（描述了特定字样和风格，比如斜体风格等） */
	int32_t     mono;    /* 是否为二值化模式 */
} ft_fontinfo;

/* 字模格式常量定义 */
typedef enum _glyph_format_t {
	GLYPH_FMT_ALPHA, /* 每个像素占用1个字节 */
	GLYPH_FMT_MONO,  /* 每个像素占用1个比特 */
} glyph_format_t;

/* 字模（位图） */
typedef struct _glyph_t {
	int16_t  x;
	int16_t  y;
	uint16_t w;
	uint16_t h;
	uint16_t advance;  /* 占位宽度 */
	uint8_t  format;   /* 字模格式 */
	uint8_t  pitch;    /* 跨距（每行像素个数 * 单个像素所占字节数） */
	uint8_t* data;    /* 字模数据：每个像素点占用一个字节 */
	void* handle;  /* 保存需要释放的句柄 */
} glyph_t;

/* 获取二值化位图上像素点的值 */
uint8_t bitmap_mono_get_pixel(const uint8_t* buff, uint32_t w, uint32_t h, uint32_t x, uint32_t y) {
	if (buff == nullptr || (x > w && y > h)) {
		return 0;
	}
	/* 计算字节偏移 */
	uint32_t line_length = ((w + 15) >> 4) << 1;
	uint32_t offset = y * line_length + (x >> 3);

	/* 计算位偏移 */
	uint32_t offset_bit = 7 - (x % 8);

	const uint8_t* data = buff + offset;
	return (*data >> offset_bit) & 0x1;
}

/* 获取字模 */
static int font_ft_get_glyph(ft_fontinfo* font_info, wchar_t c, float font_size, glyph_t* g) {
	FT_Glyph glyph;
	FT_GlyphSlot glyf;
	FT_Int32 flags = FT_LOAD_DEFAULT | FT_LOAD_RENDER;
	// 小字
	flags = FT_LOAD_NO_HINTING | FT_LOAD_MONOCHROME | FT_OUTLINE_HIGH_PRECISION;
	

	if (font_info->mono) {
		// flags |= FT_LOAD_TARGET_MONO;
		// flags = FT_LOAD_TARGET_MONO | FT_LOAD_NO_HINTING | FT_LOAD_MONOCHROME | FT_OUTLINE_HIGH_PRECISION;
	}
	/* 设置字体大小 */
	// FT_Set_Char_Size(font_info->face, 0, font_size * 64, 0, 96);
	FT_Set_Pixel_Sizes(font_info->face, 0, font_size);

	// 斜体
	if (true) {
		// 倾斜度，越大就越斜
		float lean = 0.5f;
		FT_Matrix m;
		m.xx = 0x10000L;
		m.xy = lean * 0x10000L;
		m.yx = 0;
		m.yy = 0x10000L;
		FT_Set_Transform(font_info->face, &m, NULL);
	}

	/* 通过编码加载字形并将其转化为位图（保存在face->glyph->bitmap中） */
	if (!FT_Load_Char(font_info->face, c, flags)) {
		glyf = font_info->face->glyph;
		FT_Get_Glyph(glyf, &glyph);

		g->format = GLYPH_FMT_ALPHA;
		g->h = glyf->bitmap.rows;
		g->w = glyf->bitmap.width;
		g->pitch = glyf->bitmap.pitch;
		g->x = glyf->bitmap_left;
		g->y = -glyf->bitmap_top;
		g->data = glyf->bitmap.buffer;
		g->advance = glyf->metrics.horiAdvance/* / 64*/;

		if (g->data != NULL) {
			if (glyf->bitmap.pixel_mode == FT_PIXEL_MODE_MONO) {
				g->format = GLYPH_FMT_MONO;
			}
			g->handle = glyph;
		} else {
			FT_Done_Glyph(glyph);
		}
	}
	return (g->data != NULL || c == L' ') ? 1 : 0;
}

Texture9* createFont(int font_size, wchar_t c) {

	ft_fontinfo   font_info;         /* 字库信息 */
	long int      size = 0;          /* 字库文件大小 */
	unsigned char* font_buf = NULL;  /* 字库文件数据 */

	/* 加载字库文件存入font_buf */
	FILE* font_file = fopen("C:/Windows/Fonts/simsun.ttc", "rb");
	if (font_file == NULL) {
		printf("Can not open font file!\n");
		getchar();
		return nullptr;
	}
	fseek(font_file, 0, SEEK_END); /* 设置文件指针到文件尾，基于文件尾偏移0字节 */
	size = ftell(font_file);       /* 获取文件大小（文件尾 - 文件头  单位：字节） */
	fseek(font_file, 0, SEEK_SET); /* 重新设置文件指针到文件头 */

	font_buf = (unsigned char*)calloc(size, sizeof(unsigned char));
	fread(font_buf, size, 1, font_file);
	fclose(font_file);

	font_info.mono = 1;  /* 设置为二值化模式 */

	/* 初始化FreeType */
	FT_Init_FreeType(&(font_info.library));


	/* 从font_buf中提取外观 */
	FT_New_Memory_Face(font_info.library, font_buf, size, 0, &(font_info.face));

	/* 设置字体编码方式为UNICODE */
	FT_Select_Charmap(font_info.face, FT_ENCODING_UNICODE);


	glyph_t g;
	// wchar_t c = L'a';
	// float  font_size = 36;  /* 设置字体大小 */
	font_ft_get_glyph(&font_info, c, font_size, &g);  /* 获取字模 */

	auto texture = g_pD3D9App->Texture_Create(g.w, g.h);
	auto ptr = g_pD3D9App->Texture_Lock(texture);
	memset(ptr, 0, g.w * g.h * 4);
	/* 打印字模信息 */
	int i = 0, j = 0;
	if (g.format == GLYPH_FMT_MONO) {
		for (j = 0; j < g.h; ++j) {
			for (i = 0; i < g.w; ++i) {
				uint8_t pixel = bitmap_mono_get_pixel(g.data, g.w, g.h, i, j);
				if (pixel) {
					printf("0");
					ptr[j * g.w + i] = 0xFFFFFFFF;
				} else {
					printf(" ");
				}
			}
			printf("\n");
		}
	} else if (g.format == GLYPH_FMT_ALPHA) {
		for (j = 0; j < g.h; ++j) {
			for (i = 0; i < g.w; ++i)
				putchar(" .:ioVM@"[g.data[j * g.w + i] >> 5]);
			putchar('\n');
		}
	}

	/* 释放资源 */
	FT_Done_Glyph((FT_Glyph)g.handle);
	FT_Done_FreeType(font_info.library);
	free(font_buf);
	getchar();

	return texture;
}
