#pragma once
#include "Texture.h"
#include <math.h>
extern My2d *my2d;
// ê�����Ͻ�


struct TextureQuad : public Vertex4 {

    int x, y, width, height;

	TextureQuad() : x(0), y(0), width(0), height(0) {

	}

	void setXY(int x, int y) {
		this->x = x;
		this->y = y;
	}

	void setSize(int width, int height) {
		this->width = width;
		this->height = height;
	}

	void setRect(int x, int y, int width, int height) {
        vertexs[0].tx = vertexs[3].tx = x * 1.0f / this->width;
        vertexs[0].ty = vertexs[1].ty = y * 1.0f / this->height;
        vertexs[1].tx = vertexs[2].tx = (x + width) * 1.0f / this->width;
        vertexs[2].ty = vertexs[3].ty = (y + height) * 1.0f / this->height;

        float x1, y1, x2, y2;
        x1 = 0.0f; // -_anX * _w * _scaleX;
        y1 = 0.0f; // -_anY * _h * _scaleY;
        x2 = width; // (_w - _anX * _w)* _scaleX;
        y2 = height; // (_h - _anY * _h)* _scaleY;

        vertexs[0].x = x1 + this->x; vertexs[0].y = y1 + this->y;
        vertexs[1].x = x2 + this->x; vertexs[1].y = y1 + this->y;
        vertexs[2].x = x2 + this->x; vertexs[2].y = y2 + this->y;
        vertexs[3].x = x1 + this->x; vertexs[3].y = y2 + this->y;
	}
};





struct Sprite
{
	Sprite(){}
	Sprite(int tx, int ty, int w, int h){ Sprite(Texture(), tx, ty, w, h); }

	Sprite(Texture tex, int tx = 0, int ty = 0, int w = 0, int h = 0)
	{
		_tx = tx, _ty = ty;
		setTexture(tex);
		if (w != 0 && h != 0)
			setWH(w, h);
		// _quad.vertexs[0].z = _quad.vertexs[1].z = _quad.vertexs[2].z = _quad.vertexs[3].z = 0.5f;
		setColor(_color);
		setBlend(_blend);
		setXY();
	}

	void render()
	{
		my2d->gfxQuad(&_quad, _texture);
	}

	void setTexture(Texture tex, bool size = true)
	{
		_texture = tex;
		// _quad.tex = _texture.texture;
		_quad.setSize(_texture.tw, _texture.th);
		if (size)
		{
			setWH(_texture.w, _texture.h);
		}
		else
		{
			setWH();
		}
	}

	IDirect3DTexture9* getTexture(){ return _texture; }

	void setColor(ulong color){ /*_quad.vertexs[0].color = _quad.vertexs[1].color = _quad.vertexs[2].color = _quad.vertexs[3].color = _color = color;*/ }
	ulong getColor(){ return _color; }
	void setBlend(int blend) { /*_quad.blend = _blend = blend;*/ }
	int getBlend(){ return _blend; }
	int getW(){ return _w; }
	int getH(){ return _h; }
	void setW(int w){ _w = w; setWH(); }
	void setH(int h){ _h = h; setWH(); }
	void setWH(int w, int h){ _w = w, _h = h; setWH(); }

	void setWH()
	{
		_quad.vertexs[0].tx = _quad.vertexs[3].tx = _tx * 1.0f / _texture.tw;
		_quad.vertexs[0].ty = _quad.vertexs[1].ty = _ty * 1.0f / _texture.th;
		_quad.vertexs[1].tx = _quad.vertexs[2].tx = (_tx + _w) * 1.0f / _texture.tw;
		_quad.vertexs[2].ty = _quad.vertexs[3].ty = (_ty + _h) * 1.0f / _texture.th;
	}

	int getTX(){ return _tx; }
	int getTY(){ return _ty; }
	void setTX(int x){ _tx = x; setWH(); }
	void setTY(int y){ _ty = y; setWH(); }
	void setTXY(int x, int y){ _tx = x, _ty = y; setWH(); }

	void setRect(int x, int y, int w, int h){ _tx = x, _ty = y; _w = w, _h = h; setWH(); }



	int getX(){ return _x; }
	int getY(){ return _y; }
	void setX(int x) { _x = x; _quad.x = x; setXY(); }
	void setY(int y) { _y = y; _quad.y = y; setXY(); }
	void setXY(int x, int y) { _x = x, _y = y;  _quad.setXY(x, y); setXY(); }

	void setXY()
	{
		float x1, y1, x2, y2;
		float sint, cost;
		x1 = -_anX * _w * _scaleX;
		y1 = -_anY * _h * _scaleY;
		x2 = (_w - _anX * _w) * _scaleX;
		y2 = (_h - _anY * _h) * _scaleY;

		if (_rotate != 0.0f)
		{
			cost = cosf(_rotate * PI / 180);
			sint = sinf(_rotate * PI / 180);

			_quad.vertexs[0].x = x1 * cost - y1 * sint + _x;
			_quad.vertexs[0].y = x1 * sint + y1 * cost + _y;

			_quad.vertexs[1].x = x2 * cost - y1 * sint + _x;
			_quad.vertexs[1].y = x2 * sint + y1 * cost + _y;

			_quad.vertexs[2].x = x2 * cost - y2 * sint + _x;
			_quad.vertexs[2].y = x2 * sint + y2 * cost + _y;

			_quad.vertexs[3].x = x1 * cost - y2 * sint + _x;
			_quad.vertexs[3].y = x1 * sint + y2 * cost + _y;
		}
		else
		{
			_quad.vertexs[0].x = x1 + _x; _quad.vertexs[0].y = y1 + _y;
			_quad.vertexs[1].x = x2 + _x; _quad.vertexs[1].y = y1 + _y;
			_quad.vertexs[2].x = x2 + _x; _quad.vertexs[2].y = y2 + _y;
			_quad.vertexs[3].x = x1 + _x; _quad.vertexs[3].y = y2 + _y;
		}
	}

	void setRotation(int ro){ _rotate = ro; setXY(); }
	int getRotation(){ return _rotate; }
	void setScaleX(float x){ _scaleX = x; setXY(); }
	void setScaleY(float y){ _scaleY = y; setXY(); }
	void setScale(float x, float y){ _scaleX = x, _scaleY = y; setXY(); }
	void setScale(float sc){ _scaleX = _scaleY = sc; setXY(); }
	float getScaleX(){ return _scaleX; }
	float getScaleY(){ return _scaleY; }

	void setAnX(float x){ _anX = x; setXY(); }
	void setAnY(float y){ _anY = y; setXY(); }
	void setAnXY(float x, float y){ _anX = x; _anY = y; setXY(); }
	void setAnXY(float a){ _anX = _anY = a; setXY(); }

	float getAnX(){ return _anX; }
	float getAnY(){ return _anY; }

	int _x = 0, _y = 0, _w = 0, _h = 0, _tx = 0, _ty = 0;
	int	_rotate = 0, _blend = BLEND::D;
	float _scaleX = 1, _scaleY = 1, _anX = 0, _anY = 0;
	Texture _texture;
	ulong _color = 0xffffffff;
	TextureQuad _quad;
};

