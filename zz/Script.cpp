#include "Script.h"
#include "escript.h"
#include "Obj.h"
#include "Scene.h"
#include "TaskScript.h"
#include "uiDialog.h"


void cScript::Say(cObj* m_pObj) {
	sAction* pAction = GetCommand(p);
	m_DialogueStyle = pAction->pEntryUnoins[2].intData;
	//正规对话
	if (m_DialogueStyle == -1) {
		g_Dialog.Reset();
		g_Dialog.SetObj(g_Scene.FindObjByName(pAction->pEntryUnoins[0].stringData));
		g_Dialog.m_pObj = p;

		auto& List = g_Dialog.m_ContextList;
		// List.SetFaceText(pAction->pEntryUnoins[1].stringData, g_uiManager.m_hFont[0], 470);
		List.SetString(k0U, pAction->pEntryUnoins[1].stringData);
		g_uiManager.InterfaceSetShow(&g_Dialog, true);

#if 0
		//设置位置
		int  y = g_Dialog.m_DialogBack.m_y + 20;
		int xStart = g_Dialog.m_DialogBack.m_x + 5;
		
		for (int i = 0; i < List.m_LineNum; i++) {
			y += List.m_TextLineList[i].m_Height + 2;
			cText& text = List.m_TextLineList[i].m_line;
			text.SetXY(xStart, y - 18);
			if (1 == List.m_LineNum && 0 == List.m_TextLineList[i].m_NumFace) {
			} else {
				//如果有表情,先显示表情
				for (int j = 0; j < List.m_TextLineList[i].m_NumFace; j++) {
					List.m_TextLineList[i].m_Face[j].m_x = xStart + List.m_TextLineList[i].m_xFace[j];
					List.m_TextLineList[i].m_Face[j].m_y = y + 2;
				}
			}
		}
#endif
	}

	//
	if (m_DialogueStyle > -1) {
		g_uiChannel.m_pCurrentChannel = &g_uiChannel.m_Channel[m_DialogueStyle];
		m_SleepTime = 210;
		g_uiChannel.AddNewString(pAction->pEntryUnoins[1].stringData, pAction->pEntryUnoins[0].stringData, pAction->pEntryUnoins[2].intData);
		//Sleep(60);
	}

	/*p->m_Script.*/m_NowIndex += 1;
	return false;
}