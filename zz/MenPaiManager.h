#pragma once
#include "cplus.h"

//代表一个技能
struct sSkill {
	string m_Name;
	int    m_Id = 0;
	string m_Text;//描述
	string m_tCost;//消耗的描述
	string m_tCostHpMp;
	int    m_Cost = 0;
	uint  m_BigHead = 0;
	uint  m_SmallHead = 0;
	int   m_SkillType = 0;
	int   m_EffectType = 0;
	int   m_Owner7 = -1; //位置
	uint m_State = 0;
	uint  m_Sound = 0;
	int m_NumUnCompatible = 0;
	vector<int> m_UnCompatible;
	uint  m_MagicID = 0;  //法术动画
	int  m_MagicPos = 0; //动画的位置

	int  m_TargetNumMax = 0;//目标最大数 比如龙卷最多秒7 那个这数就是7
	int  m_TargetNumOffset = 0;//多少级目标数+1   比如龙卷雨击25级多一个目标
	friend class cSkillManager;
};



//代表一个主技能
class cZhuJiNeng
{
public:
	int m_ID; //主技能ID
	string m_Name;
	uint m_SmallHead;
	uint m_BigHead;
	string m_Text;//描述  包含什么技能
	string m_Text2;//描述2   学习效果
	//包含技能
	int m_NumSkillInclude;
	// 原本12
	std::array<int, 14> m_Limit技能解锁;
	std::array<int, 14> m_SkillIncludeID;

};
//代表一个门派
class cMengPai
{
public:
	int m_ID;
	string m_Name;			//门派的名字
	string m_ShiFuName;    //门派师父的名字
	string m_ShiFuMap; //门派师父所在地图
	string m_XunLuoMapName;//师门巡逻的地点
	std::array<cZhuJiNeng, 7> m_MainSkill;//主技能
};


class cMengPaiManager
{
public:
	static const int c_SkillNum = 207;
	bool Init();
	cMengPai* GetMengPai(int menpai);
	sSkill* GetSkill(int ID);
	std::array<cMengPai, 15> m_MengPai;//门派表
	std::array<sSkill, c_SkillNum> m_SkillList;//师门技能表
private:
	bool m_bInit = false;
};

extern cMengPaiManager g_MengPaiManager;