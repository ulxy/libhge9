#include "NpcManager.h"
#include "constant.h"
#include "macro.h"
#include "TxtNpc.h"

cNpcManager g_NpcManager;

cNpcManager::cNpcManager() {

}


void cNpcManager::init() {
    if (_loaded) {
        return;
    }
    _loaded = true;

    const auto& npcs = txt::getNpcs();
    m_pNpcList.resize(npcs.size());
    forv(npcs, k) {
        const auto& n = npcs[k];
        auto& npc = m_pNpcList[k];
        npc.m_ModeId = (int)n.e + k_1000;
        npc.m_name = n.name;
        npc.m_dialog = n.dialog;
        npc.m_stand = n.stand;
        npc.m_walk = n.walk;
    }
}

