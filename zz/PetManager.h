#pragma once
#include "modata.h"

class cPetManager
{
public:
	cPetManager();
	bool Init();

	sPetData* GetPetData(int modeid);

private:
	bool m_bInit = false;
public:
	vector<sPetData> m_PetList;
};


extern cPetManager g_PetManager;