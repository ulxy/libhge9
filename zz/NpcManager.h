#pragma once
#include "npc.h"

struct sNpcData {
	int  m_ModeId;
	string  m_name;
	uint	m_stand;
	uint	m_walk;
	uint m_dialog;
};


class cNpcManager {
public:
	cNpcManager();
	void init();
public:
	vector<sNpcData> m_pNpcList;
private:
	bool _loaded = false;
};


extern cNpcManager g_NpcManager;