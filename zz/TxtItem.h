#pragma once
#include "txtinc.h"
#include "eitem.h"
#include "integer.h"

namespace txt
{
	struct sItem
	{
		eItem e;
		Integer id;
		uint small;
		uint big;
		std::string name;
		std::string desc;
	};

	const sItem& getItem(const eItem& e, int id);


	const vector<txt::sItem>& getItems();
}