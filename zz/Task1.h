#pragma once
#include "Trigger.h"

class cTask1 :public cTask {
public:
	cTask1() { Init(); }
	void Init();
	bool Process(int stage);
};

extern cTask1 g_Task1;