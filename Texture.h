#pragma once
#include "rgb.h"

// union RGB888 { unsigned int _; struct { unsigned char b, g, r, a; }; };

// d3d���
struct Vertex {
    float x, y;		// screen position    
    float z = 0.5f;	// Z-buffer depth 0..1
    unsigned long color = 0xFFFFFFFF;
    float tx, ty;	// texture coordinates
};

struct Vertex4 {
    Vertex vertexs[4/*PRIM::QUAD*/];
};

struct TextureQuad : public Vertex4 {

    int x, y, width, height;

    TextureQuad();

    void setXY(int x, int y);

    void setSize(int width, int height);

    void setRect(int x, int y, int width, int height);
};


struct IDirect3DTexture9;
class TextureCopyer {
public:
    TextureCopyer();
    TextureCopyer(const TextureCopyer& copyer);
    ~TextureCopyer();

    bool isValid() const;

    void free();

protected:
    IDirect3DTexture9* texture = nullptr;
};


class BitmapD3D9 : public TextureCopyer {
public:
    // unicode
    bool load(const char* truetype, int width, int height, uint color, bool shadow);

    bool load(int width, int height, const RGB888* texture);

    bool load(int width, int height, int row, int cel, const RGB888* texture);

    bool load(int width, int height, const uchar* indexs, const uchar* alphas, const RGB565* palatte);

    void render(int iframe, int x, int y, int x0, int y0, int width, int height, float alpha = 1.0f) const;

    void render(int x, int y, float alpha = 1.0f) const;

    void render1(int iframe, int x, int y, int width, int height) const;

    void render4(int x, int y, int x0, int y0, int width, int height) const;

    void render9(int x, int y, int width) const;

    void render9(int x, int y, int width, int height, int e) const;

public:
    int width, height, row, cel;
    mutable TextureQuad quad;
};
