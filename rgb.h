#pragma once
#include "cplus.h"

// only for transform   d3d9 bgra
union RGB888 { uint _; struct { uchar b, g, r, a; }; };
// only for transform
union RGB565 { ushort _; struct { ushort b : 5, g : 6, r : 5; }; };

union Dye4 {
    uint value = 0;
    uchar indexs[4];
};

using Dye4s = std::array<Dye4, 4>;