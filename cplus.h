#pragma once

#include <string>
#include <sstream>
#include <fstream>
#include <stdarg.h>
#include <functional>


#include <array>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <stack>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>

#if 0
#include "freee.h"
#include "macro.h"
#include "unsigned.h"
#else


template <typename T>
inline void SAFE_DELETE(T& p) { if (p) { delete p; p = nullptr; } }

template <typename T>
inline void SAFE_DELETE_ARRAY(T& p) { if (p) { delete[] p; p = nullptr; } }

template <typename T>
inline void CC_DELETE(T& p) { if (p) { delete p; p = nullptr; } }

template <typename T>
inline void CC_DELETE_ARRAY(T& p) { if (p) { delete[] p; p = nullptr; } }

template <typename T>
inline void CC_RELEASE(T& p) { if (p) { p->release(); p = nullptr; } }


#define forr(_vect_,_i_) for(int _i_ = (int)_vect_.size() - 1; _i_ >= 0; --_i_)
#define forv(_vect_,_i_) for(int _size_ = (int)_vect_.size(), _i_ = 0; _i_ < _size_; ++_i_)
#define cc_find(_vector_, _V_) (std::find(_vector_.begin(), _vector_.end(), (_V_)) != _vector_.end())

// u8
using uchar = unsigned char;
// u16
using ushort = unsigned short;
// u32
using uint = unsigned long;
// u64
using ulong = unsigned long long;

using const_string_t = const std::string&;

#endif


using std::string;
using std::vector;
using std::ostringstream;
using std::istringstream;


static const int c_1 = -1;
static const uint k0U = 0U;

static const int c640 = 640;
static const int k640 = c640;
static const int k800 = 800;
static const int k600 = 600;

static const int DIRECTION_UP = 8;
static const int DIRECTION_DOWN = 2;
static const int DIRECTION_RIGHT = 6;
static const int DIRECTION_LEFT = 4;
static const int DIRECTION_LEFT_UP = 7;
static const int DIRECTION_RIGHT_UP = 9;
static const int DIRECTION_LEFT_DOWN = 1;
static const int DIRECTION_RIGHT_DOWN = 3;

static const string c_str0;
static const string c_strSpace = " ";
static const string c_strEnter = "\n";

namespace COLOR {
    static const uint
        WHITE = 0xFFFFFF,
        BLACK = 0x000000,
        RED = 0xFF0000,
        GREEN = 0x00FF00,
        BLUE = 0x0000FF,
        YELLOW = 0xFFFF00,
        NPC = 0xE0D428,
        OBJECT = 0x6AE47E;
};


enum eWorkTye {

    WORKTYPE_ATTACK = 1,    //攻击
    WORKTYPE_MAGIC = 3,    //施法
    WORKTYPE_APPROACH,    //接近
    WORKTYPE_DEFEND,    //防
    WORKTYPE_SUFFER,    //受到物理攻击
    WORKTYPE_RETURN,    //返回
    WORKTYPE_MAGICSUFFER, //受到魔法攻击
    WORKTYPE_MAGICATTACK,  //法术驱动的物理攻击
    WORKTYPE_FLY,     //召唤兽被击飞
    WORKTYPE_FALLDOWN,  //角色死亡
    WORKTYPE_CATCH, // 抓捕
    WORKTYPE_AVOID, // 回避
    WORKTYPE_MAGIC2,    //使用特殊法术 比如防御,攻击
    WORKTYPE_ITEM,   //用道具
    WORKTYPE_SUMMON,    //召唤
    WORKTYPE_GUARD,    //保护
    WORKTYPE_COUNTER,  //反击
};

struct Vec2 { 
    int x = 0, y = 0;
    Vec2 operator+(const Vec2& v) const {
        Vec2 temp;
        temp.x = v.x + x;
        temp.y = v.y + y;
        return temp;
    }
    Vec2 operator-(const Vec2& v) const {
        Vec2 temp;
        temp.x = x - v.x;
        temp.y = y - v.y;
        return temp;
    }
    Vec2 operator*(int i) const {
        Vec2 v;
        v.x = x * 2;
        v.y = y * 2;
        return v;
    }
    Vec2 operator/(int i) const {
        Vec2 v;
        v.x = x / 2;
        v.y = y / 2;
        return v;
    }

};



#pragma warning(disable:4244) // 从“int”转换到“float”，可能丢失数据
#pragma warning(disable:4267) // “参数”: 从“size_t”转换到“int”，可能丢失数据
#pragma warning(disable:4838) // 从“const Int”转换到“char”需要收缩转换
#pragma warning(disable:4309) // “初始化”: 截断常量值

// #pragma warning(disable:4996) 
// #pragma warning(disable:4995) 
// #pragma warning(disable:4018) 


inline std::string toString(const char* format, ...) {
    const int c1024 = 8192;
    char* arr = (char*)malloc(c1024);
    memset(arr, 0, c1024);
    va_list ap;
    va_start(ap, format);
    vsnprintf_s(arr, c1024, c1024, format, ap);
    va_end(ap);
    std::string str(arr);
    free(arr);
    return str;
}