#pragma once
#include "direct3d9.h"

#include "ft2build.h"
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OUTLINE_H

#include <map>
#include <vector>

#include "tmtools.h"
#include "cchar.h"
#include "cbuffer.h"



class CFont {
public:
	CFont();
	virtual ~CFont();

	bool                    create(FT_Library library, const char* filename, FT_Long face_index, int tall, bool bold, bool italic, bool antialias);

	int                     getFontTall(void) const;

	void                    renderChar(int code, glyphMetrics* metrics, unsigned char* screen, Vec2i& position);

	void                    setColor(Color color);
	void                    setOutline(bool type);
	void                    setOutlineColor(Color color);

	string                  fontName() const;

private:

	void                    loadChar(int code, glyphMetrics* metrics, unsigned char* screen, Vec2i& position);

	void                    Draw_Bitmap(FT_GlyphSlot& glyph, unsigned char* screen, Vec2i& position, CChar* charBuffer);
	void                    Draw_Bitmap(CChar* charBuffer, unsigned char* screen, Vec2i& position);

	void                    Draw_MONO_Bitmap(FT_GlyphSlot& glyph, unsigned char* screen, Vec2i& position, CChar* charBuffer);
	void                    Draw_MONO_Bitmap(CChar* charBuffer, unsigned char* screen, Vec2i& position);

	bool                    isFontEdge(unsigned char* buffer, int row, int col, int width, int height);

	//typedef                 std::map<int, CChar *> TCharMap;
	//TCharMap                m_chars;
	CBuffer<int, CChar>     m_charBuffers;                  /**< char buffer. */


	FT_Library              m_library;                      /**< font library. */
	FT_Face                 m_face;                         /**< font face. */

	bool                    m_antialias;                    /**< font property: antialias. */
	bool                    m_bold;                         /**< font property: bold. */
	bool                    m_outline;                      /**< font property: outline. */

	Color                   m_color;                        /**< font color: font. */
	Color                   m_outline_color;                /**< font color: outline. */
	Color                   m_bg_color;                     /**< font color: background. */

	int                     m_tall;                         /**< font property: tall (or size). */
	string                  m_name;                         /**< font property: font name. */

};
